# MySql Replication

Tugas kuliah Basis Data Terdistribusi : Membuat 1 node master dan 2 node slave dan tersambung dengan replikasi.

Langkah:

**STEP 1 : Buat Direktori dan Ekstrak File Referensi**

Pertama-tama, buat sebuah folder tersendiri untuk file vagrant. Kemudian akses file tersebut dengan cmd. Setelah itu, lakukan vagrant init.
Setelah dilakukan vagrant init akan ada file vagrantfile sebagai referensi konfigurasi dari vagrant.

Setelah konfigurasi dilakukan, ekstrak file zip referensi kedalam direktori tersebut.

**STEP 2 : Modifikasi Vagrantfile**

Dengan menggunakan text editor, ubah vagrantfile menjadi seperti berikut:

```
# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.define "master" do |master|
    # Box Settings
    master.vm.box = "bento/ubuntu-16.04"
    master.vm.hostname = "master"

    # Provider Settings
    master.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      vb.name = "master"
      # Customize the amount of memory on the VM:
      vb.memory = "512"
    end

    # Network Settings
    # master.vm.network "forwarded_port", guest: 80, host: 8080
    # master.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
    master.vm.network "private_network", ip: "192.168.33.100"
    # master.vm.network "public_network"

    # Folder Settings
    # master.vm.synced_folder "../data", "/vagrant_data"

    # Provision Settings
    # master.vm.provision "shell", inline: <<-SHELL
    #   apt-get update
    #   apt-get install -y apache2
    # SHELL
    master.vm.provision "shell", path: "provision/bootstrap-master.sh", privileged: false
  end
  
  (1..2).each do |i|
	config.vm.define "slave#{i}" do |slave|
		# Box Settings
		slave.vm.box = "bento/ubuntu-16.04"
		slave.vm.hostname = "slave#{i}"

		# Provider Settings
		slave.vm.provider "virtualbox" do |vb|
		# Display the VirtualBox GUI when booting the machine
		vb.gui = false
		vb.name = "slave#{i}"
		# Customize the amount of memory on the VM:
		vb.memory = "512"
		end

		# Network Settings
		# slave.vm.network "forwarded_port", guest: 80, host: 8080
		# slave.vm.network "forwarded_port", guest: 80, host: 8080, host_ip: "127.0.0.1"
		slave.vm.network "private_network", ip: "192.168.33.10#{i}"
		# slave.vm.network "public_network"

		# Folder Settings
		# slave.vm.synced_folder "../data", "/vagrant_data"

		# Provision Settings
		# slave.vm.provision "shell", inline: <<-SHELL
		#   apt-get update
		#   apt-get install -y apache2
		# SHELL
		slave.vm.provision "shell", path: "provision/bootstrap-slave#{i}.sh", privileged: false
		end
	end
end
```
Pada `vagrantfile` ini dilakukan beberapa perubahan berikut:
1.  mengubah alokasi memori menjadi 512mb. Hal ini dapat dilakukan dengan mengubah nilai pada `vb.memory` menjadi 512.
2.  menambahkan satu lagi server slave. Hal ini dapat dilakukan dengan membuat perintah do kepada konfigurasi server slave dengan perintah `(1..2)each do |i|`,
dengan i sebagai variabel. Dari sini beberapa konfigurasi akan dirubah sesuai dengan nilai i, yaitu `config.vm.define`, `config.vm.hostname`, `vb.name`, dan `slave.vm.network`.
3.  mengubah file provision. Dari awalnya hanya ada 1 file menjadi 2 file untuk masing-masing slave.

**STEP 2 - Provisioning**

Pada langkah kedua, semua file provision akan berada dalam folder provision. Dalam langkah ini, hanya kedua file provision milik slave yang akan dimodifikasi dari file referensi.

Berikut adalah file provision `bootstrap-slave1.sh`
```
# Changing the APT sources.list to kambing.ui.ac.id
sudo cp '/vagrant/config/sources.list' '/etc/apt/sources.list'

# Updating the repo with the new sources
sudo apt-get update -y

# Setting input for MySQL root password
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password admin'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password admin'

# Install MySQL Server
sudo apt-get install mysql-server mysql-client -y

# Copy MySQL configuration
sudo cp '/vagrant/config/mysqld-slave1.cnf' '/etc/mysql/mysql.conf.d/mysqld.cnf'

# Restart MySQL service
sudo service mysql restart
```

Kemudian file provision dari `bootstrap-slave2.sh`
```
# Changing the APT sources.list to kambing.ui.ac.id
sudo cp '/vagrant/config/sources.list' '/etc/apt/sources.list'

# Updating the repo with the new sources
sudo apt-get update -y

# Setting input for MySQL root password
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password admin'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password admin'

# Install MySQL Server
sudo apt-get install mysql-server mysql-client -y

# Copy MySQL configuration
sudo cp '/vagrant/config/mysqld-slave2.cnf' '/etc/mysql/mysql.conf.d/mysqld.cnf'

# Restart MySQL service
sudo service mysql restart
```

Dari kedua file ini yang diubah hanyalah file konfigurasi mana yang akan dikopikan menjadi file `mysqld.cnf`.

**Step 3 : Configuration**

Pada langkah ketiga, sama seperti file referensi. Hanya saja ada beberapa hal yang harus dirubah dari kedua file konfigurasi slave.
Ubahlah string `bind-address` menjadi alamat IP dari server slave tersebut, dan ubah `server-id` menjadi angka yang unik. Dalam kasus ini slave pertama akan diberi id  2, sedangkan slave kedua 3.

**Step 4 : Vagrant**

Lakukan `vagrant up` pada direktori project menggunakan cmd.

Setelah proses selesai, lakukan langkah selanjutnya menggunakan cmd, yaitu:

**Step 5 : Konfigurasi Server Master**

Pertama buka server master dengan perintah `vagrant ssh master` pada cmd. Kemudian akses mysql pada master
```
mysql -u root -p
```

Kemudian berikan privilege pada slave. Perintah ini dapat digunakan untuk memberi nama user slave dan passwordnya. Perintahnya sebagai berikut:
```
GRANT REPLICATION SLAVE ON *.* TO 'username'@'%' IDENTIFIED BY 'password'
```

Kemudian lakukan `FLUSH PRIVILEGES;`.

Selanjutnya buat database yang ingin direplikasi. Kemudian buat mysql menggunakan database itu dengan perintah `USE [namadb];`.
Kunci database tersebut menggunakan perintah
```
FLUSH TABLES WITH READ LOCK;
```
kemudian buka status master menggunakan perintah
```
SHOW MASTER STATUS
```
Disini akan terlihat dari posisi mana slave akan memulai replikasi. Catat nama log dan nomor posisi untuk diisi pada konfigurasi slave nanti.

Dari sini, buka window cmd baru, lakukan ssh ke master, dan eksport file database mysql menggunakan perintah berikut
```
mysqldump -u root -p --opt namadb > namadb.sql
```
Kemudian kirim filenya ke masing-masing slave dengan perintah 
```
scp namadb.sql [ip slave]:
```
dengan password: vagrant

Buka kembali window yang menampilkan tabel master, dan buka kembali tabel master menggunakan perintah `UNLOCK TABLES;`

**Step 6: Konfigurasi Slave**

Pertama, ssh ke salah satu server slave, kemudian masuk ke sqlnya. Kemudian buat 1 database sebagai wadah untuk mengimpor database yang telah diekspor dari server master.
Kemudian, keluar dari mysql dan lakukan impor database dari file sql yang sudah ada menggunakan perintah berikut:
```
mysql -u root -p namadbslave > namadb.sql
```

Setelah melakukan impor sql, masuk ke sql kembali dan lakukan perintah berikut untuk melakukan konfigurasi master pada slave:
```
CHANGE MASTER TO 
    MASTER_HOST='12.34.56.789',
    MASTER_USER='username',
    MASTER_PASSWORD='password',
    MASTER_LOG_FILE='nama file log',
    MASTER_LOG_POS= nomor posisi log;
```

Kemudian aktifkan server slave dengan perintah `START SLAVE;`

Lakukan hal berikut kepada server slave lainnya